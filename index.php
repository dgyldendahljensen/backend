<?php
/**
 * Created by PhpStorm.
 * Player: Dennis
 * Date: 29-05-2020
 * Time: 13:34
 */

declare(strict_types=1);
require_once 'vendor/autoload.php';
include('Player.php');
include 'connection.php';

dispatch('/', function () {
    return "Hello";
});

dispatch_post('/upload', function () {
    global $mysqli;
    $entityBody = file_get_contents('php://input');
    $exploded_arr = explode(";", $entityBody);
    $name = $exploded_arr[0];
    $faction = $exploded_arr[1];
    $server = $exploded_arr[2];

    $player = getOrCreateUser($name, $faction, $server);
    addRecipeIds($exploded_arr, $player, $mysqli);
});

dispatch_get('/search', function () {
    global $mysqli;

    if (array_key_exists('query', $_GET) && array_key_exists('server', $_GET)) {
        $query_string = $_GET['query'];
        $is_exact = array_key_exists('exact', $_GET) ? $_GET['exact'] : false;
        $server = $_GET['server'];
        if ($is_exact) {
            $search_str = <<<SQL
                SELECT r.spellId as id, r.name
                FROM prof_recipe as r 
                WHERE r.name = '$query_string' LIMIT 1
SQL;

        } else {
            $search_str = <<<SQL
                SELECT r.spellId as id, r.name 
                FROM prof_recipe as r 
                WHERE r.name LIKE '%$query_string%' LIMIT 5
SQL;
        }

        $search_stmt = $mysqli->prepare($search_str);

        if ($search_stmt->execute()) {
            $search_result = $search_stmt->get_result();
            $search_stmt->close();

            if ($search_result->num_rows > 0) {
                $recipes = array();
                while ($search_row = $search_result->fetch_assoc()) {
                    $recipes[] = $search_row;
                }

                if (count($recipes) == 1) {
                    $recipe = $recipes[0];
                    $recipe_id = $recipe['id'];
                    $find_players_str = <<<SQL
                        SELECT p.name
                        FROM prof_player AS p JOIN prof_player_recipe_list AS pr ON p.id = pr.player_id
                        WHERE pr.recipe_id = '$recipe_id' AND p.server = (
                          SELECT s.id FROM prof_server as s WHERE s.name = '$server' 
                        ) LIMIT 10
SQL;
                    $find_stmt = $mysqli->prepare($find_players_str);

                    if ($find_stmt->execute()) {
                        $find_result = $find_stmt->get_result();
                        $find_stmt->close();
                        $players = array();
                        while ($find_row = $find_result->fetch_assoc()) {
                            $players[] = $find_row;
                        }
                        if (count($players) != 0){
                            $result['players'] = $players;
                        }
                        $result['recipe'] = $recipe;
                        return json_encode($result);
                    }
                } else if (count($recipes) > 1) {
                    $result['list'] = $recipes;
                    return json_encode($result);
                }
            }
        } else {
            echo $search_stmt->error;
        }
    }
    return json_encode(array());
});

run();

function addRecipeIds(array $exploded_arr, Player $player, mysqli $mysqli)
{
    $recipe_arr = array();
    foreach ($exploded_arr as $recipe_id) {
        if (is_numeric($recipe_id)) {
            array_push($recipe_arr, $recipe_id);
        }
    }
    $player->setRecipeIds($recipe_arr);

    $delete_recipes_sql = <<<SQL
        DELETE FROM prof_player_recipe_list
        WHERE player_id = ?
SQL;
    $delete_recipes_stmt = $mysqli->prepare($delete_recipes_sql);
    $delete_recipes_stmt->bind_param("s", $player->getId());
    if ($delete_recipes_stmt->execute()) {
        $sql_part_str = implode(',' . $player->getId() . '),(', $player->getRecipeIds()) . ',' . $player->getId();
        $insert_recipes_sql = "INSERT INTO prof_player_recipe_list (recipe_id, player_id) VALUES ($sql_part_str)";
        $insert_recipes_stmt = $mysqli->prepare($insert_recipes_sql);
        if ($insert_recipes_stmt->execute()) {
            $insert_recipes_stmt->close();
        } else {
            throw new Exception($insert_recipes_stmt->error);
        }
    } else {
        echo $delete_recipes_stmt->error;
    }
    $delete_recipes_stmt->close();
}

function getOrCreateUser(string $name, string $faction, string $server)
{
    global $mysqli;
    $check_sql = <<<SQL
        SELECT p.id, p.name, f.name AS faction, s.name AS server 
        FROM prof_player p 
        INNER JOIN prof_server s ON p.server = s.id
        INNER JOIN prof_faction f ON p.faction = f.id
        WHERE p.name = ? AND s.name = ? 
SQL;

    $check_stmt = $mysqli->prepare($check_sql);
    $check_stmt->bind_param("ss", $name, $server);
    $check_stmt->execute();
    $check_result = $check_stmt->get_result();
    $check_stmt->close();
    echo $name . $faction . $server;
    if ($check_result->num_rows == 0) {
        $insert_sql = <<<SQL
            INSERT INTO prof_player (name, faction, server)
            VALUES (?, (SELECT id FROM prof_faction WHERE name = ?), (SELECT id FROM prof_server WHERE name = ?));
SQL;
        $insert_stmt = $mysqli->prepare($insert_sql);
        $insert_stmt->bind_param("sss", $name, $faction, $server);
        if ($insert_stmt->execute()) {
            $player_id = $insert_stmt->insert_id;
            $insert_stmt->close();
            return new Player($player_id, $name, $faction, $server);
        } else {
            $error = $insert_stmt->error;
            $insert_stmt->close();
            throw new Exception($error);
        }
    } else {
        $check_array = $check_result->fetch_assoc();
        return new Player((int)$check_array['id'], $check_array['name'], $check_array['faction'], $check_array['server']);
    }
}