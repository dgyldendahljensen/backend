<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 29-05-2020
 * Time: 15:47
 */

class Player
{
    private $id;
    private $name;
    private $faction;
    private $server;
    private $recipe_ids = array();

    /**
     * Player constructor.
     * @param $id
     * @param $name
     * @param $faction
     * @param $server
     */
    public function __construct(int $id, string $name, string $faction, string $server)
    {
        $this->id = $id;
        $this->name = $name;
        $this->faction = $faction;
        $this->server = $server;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFaction(): string
    {
        return $this->faction;
    }

    /**
     * @param string $faction
     */
    public function setFaction(string $faction): void
    {
        $this->faction = $faction;
    }

    /**
     * @return string
     */
    public function getServer(): string
    {
        return $this->server;
    }

    /**
     * @param string $server
     */
    public function setServer(string $server): void
    {
        $this->server = $server;
    }

    /**
     * @return array
     */
    public function getRecipeIds(): array
    {
        return $this->recipe_ids;
    }

    /**
     * @param array $recipe_ids
     */
    public function setRecipeIds(array $recipe_ids): void
    {
        $this->recipe_ids = $recipe_ids;
    }
}